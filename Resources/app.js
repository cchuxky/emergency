/*
 * A tabbed application, consisting of multiple stacks of windows associated with tabs in a tab group.  
 * A starting point for tab-based application with multiple top-level windows. 
 * Requires Titanium Mobile SDK 1.8.0+.
 * 
 * In app.js, we generally take care of a few things:
 * - Bootstrap the application with any data we need
 * - Check for dependencies like device type, platform version or network connection
 * - Require and open our top-level UI component
 *  
 */

//bootstrap and check dependencies
if (Ti.version < 1.8 ) {
	alert('Sorry - this application template requires Titanium Mobile SDK 1.8 or later');
}

// This is a single context application with mutliple windows in a stack
(function() {
	//determine platform and form factor and render approproate components
	var osname = Ti.Platform.osname,
		version = Ti.Platform.version,
		height = Ti.Platform.displayCaps.platformHeight,
		width = Ti.Platform.displayCaps.platformWidth;
	
	//considering tablet to have one dimension over 900px - this is imperfect, so you should feel free to decide
	//yourself what you consider a tablet form factor for android
	var isTablet = osname === 'ipad' || (osname === 'android' && (width > 899 || height > 899));
	Titanium.UI.setBackgroundColor('#fff'); 
	var tabGroup    = Titanium.UI.createTabGroup();
	
	
	var mainWin = Titanium.UI.createWindow({
		url: 'ui/common/MainWindow.js',
		title: 'EMERGENCY',
		//backgroundColor: Ti.App.Properties.getString('backgroundColor'),
		barColor: 'red',
		//barImage: Ti.App.Properties.getString('barImage'),	
		navBarHidden: false,
		tabBarHidden: false,
	});
	var mainTab = Titanium.UI.createTab({
		title: 'Call',
		icon: '/images/phone.png',
		window: mainWin,
	});  
	
	var settingWin = Titanium.UI.createWindow({
		url: 'ui/common/SettingWindow.js',
		title: 'Profile',
		//backgroundColor: Ti.App.Properties.getString('backgroundColor'),
		//barColor: Ti.App.Properties.getString('barColor'),
		//barImage: Ti.App.Properties.getString('barImage'),	
		navBarHidden: false,
		tabBarHidden: false,
	});
	var settingTab = Titanium.UI.createTab({
		window: settingWin,
		icon: '/images/address-book.png',
		title: 'Profile',
	});  
	
	
	var aidWin = Titanium.UI.createWindow({
		url: 'ui/common/AidWindow.js',
		title: 'First Aid',
		navBarHidden: false,
		tabBarHidden: false,
	});
	var aidTab = Titanium.UI.createTab({
		window: aidWin,
		icon: '/images/aid.png',
		title: 'First Aid',
	});  

	tabGroup.addTab(mainTab); 
	tabGroup.addTab(settingTab);  
	tabGroup.addTab(aidTab);
	tabGroup.open();  

})();
