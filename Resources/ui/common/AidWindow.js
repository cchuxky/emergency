var win = Titanium.UI.currentWindow;  



var view = Ti.UI.createScrollView({
	width: '100%',
});

var title = Titanium.UI.createLabel({
	text: 'Call triple zero (000) in an emergency',
	font: {fontSize: '17ps', fontWeight: 'bold'},
	top: 5,
	width: '98%',
	textAlign: 'center',
});

var subTitle = Titanium.UI.createLabel({
	text: 'ask for ambulance, stay with the injured person and resuscitate',
	font: {fontSize: '14ps', fontWeight: 'bold'},
	top: 30,
	width: '98%',
	textAlign: 'center',
});

var image = Ti.UI.createImageView({
	image: '/images/firstaid.gif',
	width: '100%',
	top: 70,
})
view.add(title);
view.add(subTitle);
view.add(image);
win.add(view);
