function location(type) {
	var win = Ti.UI.createWindow({
		tabBarHidden:true,  
	});

	
	
	var tableview = Titanium.UI.createTableView({
		backgroundColor: 'transparent',
		top:5,
	});
	
	function dataTable() {
		
		var db = Ti.Database.install('/data/emergencyDB.sqlite', 'emergencyDB');
	
		var locData = db.execute('SELECT * FROM locations');
		
		
		var data=[];
		while (locData.isValidRow()) {
			
			var dname = locData.fieldByName('name');
			var dhour = locData.fieldByName('hours');
			var dphone = locData.fieldByName('phone');
			var daddress = locData.fieldByName('address');
			
			var row = Titanium.UI.createTableViewRow({
				height: 65,
			});
			
			 
			var name = Titanium.UI.createLabel({
				text:dname,
				font:{fontSize:'16ps',fontWeight:'bold'},
				width:'auto',
				height: 20,
				textAlign:'left',
				color: 'black',
				left:68,
				top: 2,
			});
			
			var address = Titanium.UI.createLabel({
				text:daddress,
				font:{fontSize:'14ps'},
				width:'auto',
				textAlign:'left',
				color: 'grey',
				top:22,
				left:68,
			});
			row.add(name);
			row.add(address);

			data[i] = row;
			locData.next();
		};
		tableview.setData(data);
	}
	win.add(tableview);
	dataTable();
	
}

module.exports = location;