var win = Titanium.UI.currentWindow;  


var dial = Titanium.UI.createButton({
	title: 'Dial Police',
});

	Ti.App.Properties.setString('emergency', false);

	var editButton = Titanium.UI.createButton({
		title: 'Edit'
	});
	win.setRightNavButton(editButton);

	var data = [];
	
	
	data[0] = Ti.UI.createTableViewSection({headerTitle:'Allergies'});
	data[0].add(Ti.UI.createTableViewRow({title:'No known allergy', hasChild: true}));
	
	
	data[1] = Ti.UI.createTableViewSection({headerTitle:'Personal Details'});
	
	var personal = Ti.UI.createTableViewRow({height: 40});
		var name = Ti.UI.createLabel({
			text: 'FIRST NAME',
			font: {fontSize: '13ps'},
			left: 5,
		});
		var nameInfo = Ti.UI.createLabel({
			text: 'Chai',
			font: {fontWeight: 'bold', fontSize: '13ps'},
			left: 100,
		});
	personal.add(name);
	personal.add(nameInfo);
	
	var personal2 = Ti.UI.createTableViewRow({height: 40});
			var last_name = Ti.UI.createLabel({
			text: 'LAST NAME',
			font: {fontSize: '13ps'},
			left: 5,
		});
		var last_nameInfo = Ti.UI.createLabel({
			text: 'Brown Latte',
			font: {fontWeight: 'bold', fontSize: '13ps'},
			left: 100,
		});
	personal2.add(last_name);
	personal2.add(last_nameInfo);
	
	var gender = Ti.UI.createTableViewRow({height: 40});
			var gender1 = Ti.UI.createLabel({
			text: 'GENDER',
			font: {fontSize: '13ps'},
			left: 5,
		});
		var gender2 = Ti.UI.createLabel({
			text: 'Male',
			font: {fontWeight: 'bold', fontSize: '13ps'},
			left: 100,
		});
	gender.add(gender1);
	gender.add(gender2);
	
	
	var age = Ti.UI.createTableViewRow({height: 40});
			var age1 = Ti.UI.createLabel({
			text: 'AGE',
			font: {fontSize: '13ps'},
			left: 5,
		});
		var age2 = Ti.UI.createLabel({
			text: '30',
			font: {fontWeight: 'bold', fontSize: '13ps'},
			left: 100,
		});
	age.add(age1);
	age.add(age2);
	
	
	var blood = Ti.UI.createTableViewRow({height: 40});
			var blood1 = Ti.UI.createLabel({
			text: 'BLOOD TYPE',
			font: {fontSize: '13ps'},
			left: 5,
		});
		var blood2 = Ti.UI.createLabel({
			text: 'B',
			font: {fontWeight: 'bold', fontSize: '13ps'},
			left: 100,
		});
	blood.add(blood1);
	blood.add(blood2);
	
	
	
	data[1].add(personal);
	data[1].add(personal2);
	data[1].add(gender);
	data[1].add(age);
	data[1].add(blood);
	
	data[2] = Ti.UI.createTableViewSection({headerTitle:'Contacts'});
	data[2].add(Ti.UI.createTableViewRow({title:'Jane Brown', hasChild: true}));
	data[2].add(Ti.UI.createTableViewRow({title:'James Brown', hasChild: true}));
	data[2].add(Ti.UI.createTableViewRow({title:'Bruce Willis', hasChild: true}));
	
	
	data[3] = Ti.UI.createTableViewSection({headerTitle:'Phone Unlock Code'});
	data[3].add(Ti.UI.createTableViewRow({title:'1234', hasChild: true}));
	// create table view
	var tableview = Titanium.UI.createTableView({
		data:data,
		style: Titanium.UI.iPhone.TableViewStyle.GROUPED
	});
	

win.add(tableview);

