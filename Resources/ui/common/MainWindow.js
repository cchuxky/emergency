var win = Titanium.UI.currentWindow;  

if (Ti.Geolocation.locationServicesEnabled) {
	Ti.Geolocation.purpose = 'Let your friend know your location in an emergency.';
	
	Ti.Geolocation.getCurrentPosition(function(e) {
			if (e.error) {
			    Ti.API.error('geo - current position' + e.error);
			    return;
			 }
 
        	Ti.App.Properties.setString('lat', e.coords.latitude);
        	Ti.App.Properties.setString('lon', e.coords.longitude);
    });

	
	
} else {
	alert('Please enable location service');
}

var header = Ti.UI.createLabel({
	text: 'Think first, then call',
	font: {fontSize: '23ps', fontWeight:'bold'},
	top: 7,
})

win.add(header);

var triple0 = Titanium.UI.createButton({
	title: 'Call Emergency 000',
	width: '90%',
	height: 60,
	top: 50,
	backgroundColor: 'red',
	style: 'none',
	borderRadius: 6,
});

var police = Titanium.UI.createButton({
	title: 'Call Police Assistance 131 444',
	width: '90%',
	height: 60,
	top: 125,
	backgroundColor: 'blue',
	style: 'none',
	borderRadius: 6,
});

var policeStation = Titanium.UI.createButton({
	title: 'Show Nearest Police Station',
	width: '90%',
	height: 60,
	top: 200,
});

var emergencyServices = Titanium.UI.createButton({
	title: 'Show Nearest Emergency Services',
	width: '90%',
	height: 60,
	top: 275,
});

triple0.addEventListener('click', function() {
	Ti.API.info('calling..');
	Ti.App.idleTimerDisabled = true;
	Ti.App.Properties.setString('emergency', true);
	Ti.Platform.openURL('tel:0402752280');
	
    if	(Titanium.Platform.name == 'iPhone OS' && parseInt(Titanium.Platform.version.split(".")[0], 10) >=4) {
        Ti.App.iOS.registerBackgroundService({
            url:'/bg.js'
        });
    }

});

police.addEventListener('click', function() {
	Ti.API.info('calling..');
	Ti.App.idleTimerDisabled = true;
	Ti.App.Properties.setString('emergency', true);
	Ti.Platform.openURL('tel:0402752280');
	
    if	(Titanium.Platform.name == 'iPhone OS' && parseInt(Titanium.Platform.version.split(".")[0], 10) >=4) {
        Ti.App.iOS.registerBackgroundService({
            url:'/bg.js'
        });
    }

});

var service;

Ti.App.addEventListener('resumed',function(e){
	Ti.API.info("app has resumed from the background");
	// this will unregister the service if the user just opened the app
	// is: not via the notification 'OK' button..
	// sending sms
	
	if (Ti.App.Properties.getString('emergency') == true) {
	
		var smsModule = require('com.omorandi');
		
		if (Ti.Geolocation.locationServicesEnabled) {
			Ti.Geolocation.purpose = 'Let your friend know your location in an emergency.';
		
			Ti.Geolocation.getCurrentPosition(function(e) {
				if (e.error) {
				    Ti.API.error('geo - current position' + e.error);
				    return;
				 }
	
				var address;
				Titanium.Geolocation.reverseGeocoder(Ti.App.Properties.getString('lat'), Ti.App.Properties.getString('lon'), function(evt) {
	            	if (evt.success) {
		                var places = evt.places;
		               
		                if (places && places.length) {
		                    address = places[0].address;
		                }
		            }
		        });
		        
		        //if (address == 'undefined' || address == ''){
					address = 'http://maps.google.com/maps?daddr='+e.coords.latitude+','+e.coords.longitude;
				//}		
				sms = smsModule.createSMSDialog({
					recipients: ['0402752280', '123456789'],
					messageBody: 'Chhai is in an emergency. '+address,
				});
				sms.open({animated: true});		
						
			});
		} else {
			alert('Please enable location service');
		}
	
	} else {
		Ti.App.Properties.setString('emergency', false);
	}
	
	if(service!=null){
		service.stop();
		service.unregister();
	}
    Titanium.UI.iPhone.appBadge = null;
});

policeStation.addEventListener('click', function(){
	nearBy('South Australia Police');
});	

emergencyServices.addEventListener('click', function(){
	nearBy('South Australia State Emergency Service');
});	
function nearBy(type) {	
	
	var win2 = Ti.UI.createWindow({
		tabBarHidden:true,
	});
	
	var tableview = Titanium.UI.createTableView({
		backgroundColor: 'transparent',
		top:5,
	});
	
	function dataTable() {
		var f = Ti.Filesystem.getFile(
		Ti.Filesystem.applicationSupportDirectory, 'database', 'emergencydb.sql');
		//If it's there, delete it
		if(f.exists() == true){
		     f.deleteFile();
		     Ti.API.info('delted db');
		}
		
		var db = Ti.Database.install('/data/emergencydb.sqlite', 'emergencydb');
		
		var locData = db.execute("SELECT * FROM agency WHERE agency ='"+type+"'");
		
		
		
		var data=[];
		var i = 0;
		while (locData.isValidRow()) {
			i++;
				
				var locid = locData.fieldByName('id');
				var dname = locData.fieldByName('name');
				var dhour = locData.fieldByName('hours');
				var dphone = locData.fieldByName('phone');
				
				lat1 = Ti.App.Properties.getString('lat');
				lon1 = Ti.App.Properties.getString('lon');
				
				
				var lat2 = locData.fieldByName('lat');
				var lon2 = locData.fieldByName('lon');
				
				Ti.API.info('-> lat'+lat1+' lon 1'+lon1+ ' l2'+lat2+ ' lon2'+lon2);
				
				var daddress = locData.fieldByName('address');
				
				var d;
				if (lat1 !== '') {
					Ti.API.info('lat cal');
					Ti.API.info(lat2-lat1);
				
					var R = 6371; // km

					var dLat = toRad(lat2-lat1);
					var dLon = toRad(lon2-lon1);
					var lat1 = toRad(lat1);
					var lat2 = toRad(lat2);
					
					var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
					        Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
					var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
					d = Math.round(R * c);
				
				}
				
		
							
				//Ti.API.info(rowId);
				var row = Titanium.UI.createTableViewRow({
					height: 70,
				});
				
				row.hasChild=true;
				row.source_lat = lat1;
				row.source_lon = lon1;
				row.dest_lat = locData.fieldByName('lat');
				row.dest_lon = locData.fieldByName('lon');
				row.address = daddress;
				row.name = dname;
				 
				var name = Titanium.UI.createLabel({
					text:dname,
					font:{fontSize:'16ps',fontWeight:'bold'},
					width:'auto',
					height: 20,
					textAlign:'left',
					color: 'black',
					left:68,
					top: 4,
				});
				
				var address = Titanium.UI.createLabel({
					text:daddress,
					font:{fontSize:'14ps'},
					width:'auto',
					textAlign:'left',
					color: 'grey',
					top:25,
					left:68,
				});
				
				var distance = Titanium.UI.createLabel({
					text: d+'\nkm',
					left: 5,
					height: 60,
					width: 60,
					top: 5,
					textAlign: 'center',
					color: '#c0c0c0',
					backgroundColor: 'blue',
					borderRadius: 6,
				})
				row.add(name);
				row.add(address);
				row.add(distance);
	
				data[d] = row;
				
				locData.next();
		};
		locData.close();
		var count = 1;
		var filtered = [];
		for (var i in data) {
			
			if (count <= 5) {
				count++;
				filtered[i] = data[i];
			}
		}
		tableview.setData(filtered);
	}
	
	
	function toRad(Value) {
	    /** Converts numeric degrees to radians */
	    return Value * Math.PI / 180;
	}
	
	win2.add(tableview);
	dataTable();
	Ti.UI.currentTab.open(win2);
	
	
	var winMap = Ti.UI.createWindow({
		tabBarHidden:true,
		title: 'Map',  
	});

	
	tableview.addEventListener('click', function(e)
	{
		var rowData = e.rowData;
		
		var locationText = Titanium.Map.createAnnotation({
		    latitude:rowData.dest_lat,
		    longitude:rowData.dest_lon,
		    title:rowData.name,
		    pincolor:Titanium.Map.ANNOTATION_RED,
		    animate:true,
		    leftButton: 'images/location.png',
		    //myid:data.id // Custom property to uniquely identify this annotation.
		});
		

		var mapview = Titanium.Map.createView({
			mapType: Titanium.Map.STANDARD_TYPE,
			region:{latitude:rowData.dest_lat, longitude:rowData.dest_lon, latitudeDelta:0.001, longitudeDelta:0.001},
			animate:true,
			regionFit:true,
			userLocation:true,
			annotations:[locationText]
		});
		winMap.add(mapview);
		Ti.UI.currentTab.open(winMap);
		
	});
	
}


win.add(triple0);
win.add(police);
win.add(policeStation);
win.add(emergencyServices);
